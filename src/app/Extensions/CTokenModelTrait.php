<?php

namespace App\Extensions;

use Illuminate\Support\Str;
use Carbon\Carbon;

trait CTokenModelTrait
{
    /**
     * Get the token model class name.
     *
     * @return string
     */
    public static function tokenModel()
    {
        return config('cauth.token_model');
    }

    public function tokens() {
        return $this->morphMany(self::tokenModel(), 'user')->orderBy('created_at', 'desc');
    }

    public function revokeToken($accessToken)
    {
        $this->tokens()->where('access_token', $accessToken)->update(['revoked' => true]);
    }

    public function createToken()
    {
        $clientId = request()->clientId();

        $accessToken = Str::random(50);
        $expiresAt = Carbon::now()->addMinutes(config('cauth.token_expire_in', 15));

        $this->tokens()->delete();

        $this->tokens()->create([
            'client_id' =>  $clientId,
            'access_token' => $accessToken,
            'expires_at' => $expiresAt,
        ]);

        return [
            'access_token' => $accessToken,
            'expires_at' => $expiresAt->toIso8601String(),
        ];
    }
}