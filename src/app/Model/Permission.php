<?php

namespace App\Model;

use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
	protected $guard_name = 'api';
	
    protected $fillable = ['display_name', 'name', 'guard_name'];
}