<?php

namespace App\Model;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
	protected $guard_name = 'api';
	
    protected $fillable = ['display_name', 'name', 'guard_name'];
}