<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
	use SoftDeletes;
	
    protected $guard_name = 'api';

    protected $fillable = ['page_title', 'page_slug', 'short_description', 'description', 'status'];
}
