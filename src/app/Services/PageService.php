<?php

namespace App\Services;

use App\Model\Page;

class PageService
{
	public function __construct(Page $page)
	{
		$this->page = $page;
	}

	public function index()
	{
		return $this->page::paginate(10);
	}

	public function store($validated)
	{
		$validated['page_slug'] = str_slug($validated['page_title']);

		$page = Page::create($validated);

		return $page;
	}

	public function show($id)
	{
		return $this->page->find($id);
	}

	public function update($validated, $id)
	{
		$validated['page_slug'] = str_slug($validated['page_title']);

		$page = $this->page::find($id);
		$page->update($validated);

		return $page;
	}

	public function destroy($id)
	{
		$page = Page::find($id);

		if($page->delete()){ return $page; }
	}
}