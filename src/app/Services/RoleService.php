<?php

namespace App\Services;

use App\Model\Role;

class RoleService
{
	public function __construct(Role $role)
	{
		$this->role = $role ;
	}

	public function index()
	{
		return $this->role::paginate(10);
	}

	public function store($validated)
	{
		$validated['name'] = str_slug($validated['display_name']);

		$role = Role::create($validated);
		$role->syncPermissions($validated['permissions']);

		return $role;
	}

	public function show($id)
	{
		return $this->role->find($id);
	}


	public function update($validated, $id)
	{
		if(!empty($validated['display_name']))
		{	$validated['name'] = str_slug($validated['display_name']); }

		$role = $this->role::find($id);

		$role->update($validated);

		if(!empty($validated['permissions']))
		{	$role->syncPermissions($validated['permissions']);	}

		return $role;
	}

	public function destroy($id)
	{
		$role = Role::find($id);

		if($role->delete()){ return $role; }
	}
}
