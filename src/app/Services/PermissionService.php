<?php

namespace App\Services;

use App\Model\Permission;

class PermissionService
{
	public function __construct(Permission $permission)
	{
		$this->permission = $permission;
	}

	public function index()
	{
		return $this->permission::paginate(10);
	}

	public function store($validated)
	{
		$validated['name'] = str_slug($validated['display_name']);

		$permission = Permission::create($validated);

		return $permission;
	}

	public function show($id)
	{
		return $this->permission->find($id);
	}

	public function update($validated, $id)
	{
		$validated['name'] = str_slug($validated['display_name']);

		$permission = $this->permission::find($id);
		$permission->update($validated);

		return $permission;
	}

	public function destroy($id)
	{
		$permission = Permission::find($id);

		if($permission->delete()){ return $permission; }
	}
}