<?php

namespace App\Services;

use App\Model\User;

class UserService
{
	public function __construct(User $user)
	{
		$this->user = $user ;
	}

	public function index()
	{
		return $this->user::paginate(10);
	}

	public function store($validated)
	{
		$user = User::create($validated);
		$user->syncRoles($validated['roles']);
		
		return $user;
	}

	public function show($id)
	{
		return $this->user->find($id);
	}

	public function update($validated, $id)
	{
		$user = $this->user::find($id);

		$user->update($validated);

		if(!empty($validated['roles'])) {
			$user->syncRoles($validated['roles']);
		}

		return $user;
	}

	public function destroy($id)
	{
		$user = $this->user::find($id);

		if($user->delete()){ return $user; }
	}
}