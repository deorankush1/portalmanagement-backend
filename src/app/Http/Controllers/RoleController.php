<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleResource;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index()
    {
        $roles = $this->roleService->index();

        return RoleResource::collection($roles);
    }

    public function store(RoleRequest $request)
    {
        $validatedRole = $request->validated();

        try
        {
            $role = $this->roleService->store($validatedRole);
            
            return response()->json(new RoleResource($role), 201);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $role = $this->roleService->show($id);

        if(empty($role))
        { 
            return response()->json(['message'=>'Unable to find requested Role!'], 404);
        }

        return response()->json(RoleResource::collection($role), 200);
    }

    public function update(RoleRequest $request, $id)
    {
        $validatedRole = $request->validated(); 

        try
        {
            $role = $this->roleService->update($validatedRole, $id);

            return response()->json(new RoleResource($role), 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }        
    }

    public function destroy($id)
    {
        try
        {
            $deleteRole = $this->roleService->destroy($id);

            return response()->json(['message'=>'Role has been deleted successfully!'], 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }
}
