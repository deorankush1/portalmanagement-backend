<?php

namespace App\Http\Controllers;

use App\Services\PageService;
use App\Http\Requests\PageRequest;
use App\Http\Resources\PageResource;


class PageController extends Controller
{
	protected $pageService;

	public function __construct(PageService $pageService)
	{
		$this->pageService = $pageService;
	}

	public function index()
	{
		$pages = $this->pageService->index();

		return PageResource::collection($pages);
	}

	public function store(PageRequest $request)
    {
        $validatedPage = $request->validated();

        try
        {
            $page = $this->pageService->store($validatedPage);

            return response()->json(new PageResource($page), 201);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $page = $this->pageService->show($id);

        if(empty($page))
        { 
            return response()->json(['message'=>'Unable to find requested Page!'], 404);
        }

        return response()->json(PageResource::collection($page), 200);
    }

    public function update(PageRequest $request, $id)
    {
        $validatedPage = $request->validated();  

        try
        {
            $page = $this->pageService->update($validatedPage, $id);

            return response()->json(new PageResource($page), 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
	}

	public function destroy($id)
    {
        try
        {
            $deletePage = $this->pageService->destroy($id);

            return response()->json(['message'=>'Page has been deleted successfully!'], 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }
}
