<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

	public function index()
    {
        $users = $this->userService->index();

        return UserResource::collection($users);
    }

    public function store(UserRequest $request)
    {
        try 
        {
            $user = $this->userService->store($request->validated());

            return response()->json(new UserResource($user), 201);
        }
        catch (\Exception $ex) 
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $user = $this->userService->show($id);

        if(empty($user))
        { 
            return response()->json(['message'=>'Unable to find requested User!'], 404);
        }

        return response()->json(UserResource::collection($user), 200);
    }

    public function update(UserRequest $request, $id)
    {
        $validatedUser = $request->validated();

        try
        {
            $user = $this->userService->update($validatedUser, $id);  

            return response()->json(new UserResource($user), 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try
        {
            $deleteUser = $this->userService->destroy($id);

            return response()->json(['message'=>'User has been deleted successfully!'], 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }
}