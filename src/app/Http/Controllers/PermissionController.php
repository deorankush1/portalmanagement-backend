<?php

namespace App\Http\Controllers;

use App\Services\PermissionService;
use App\Http\Requests\PermissionRequest;
use App\Http\Resources\PermissionResource;

class PermissionController extends Controller
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $permissions = $this->permissionService->index();

        return PermissionResource::collection($permissions);
    }

	public function store(PermissionRequest $request)
	{
        $validatedPermission = $request->validated();

        try
        {
            $permission = $this->permissionService->store($validatedPermission);

            return response()->json(new PermissionResource($permission), 201);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
	}

    public function show($id)
    {
        $permission = $this->permissionService->show($id);

        if(empty($permission))
        { 
            return response()->json(['message'=>'Unable to find requested Permission!'], 404);
        }

        return response()->json(PermissionResource::collection($permission), 200);
    }

	public function update(PermissionRequest $request, $id)
    {
        $validatedPermission = $request->validated(); 

        try
        {
            $permission = $this->permissionService->update($validatedPermission, $id);

            return response()->json(new PermissionResource($permission), 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
	}

	public function destroy($id)
    {
        try
        {
            $deletePermission = $this->permissionService->destroy($id);

            return response()->json(['message'=>'Permission has been deleted successfully!'], 200);
        }
        catch (\Exception $ex)
        {
            return response()->json(['message'=>$ex->getMessage()], 500);
        }
    }	
}