<?php
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	public function adminAuthAttempt(Request $request)
	{
		$request->validate([
	        'email' => 'required|email',
	        'password' => 'required',
    	]);

		try{
			if(Auth::attempt(['email' => $request->email, 'password' => $request->password], 'App\Models\Admin')) {

				$token = Auth::user()->createToken();

				return response()->json($token, 200);

			} else {
				return response()->json(['message' => 'Invalid Credentials'], 401);
			}
		} catch(\Exception $ex) {
			return response()->json(['message' => $ex->getMessage()], 500);
		}
	}
}