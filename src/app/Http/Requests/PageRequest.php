<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            if($this->method() == 'POST')
            {
                return [
                    'page_title'        => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'short_description' => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'description'       => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'status'            => 'required|alpha'
                ];
            }

            if($this->method() == 'PUT' || $this->method() == 'PATCH')
            {
                return [
                    'page_title'        => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'short_description' => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'description'       => 'required|regex:/^[a-z\d\-_\s]+$/i',
                    'status'            => 'required|alpha'
                ];
            }
    }
}