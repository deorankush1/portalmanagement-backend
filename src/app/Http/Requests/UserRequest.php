<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user');

        if($this->method() == 'POST')
        {
            return [
                'name'      => 'required|regex:/^[a-z\d\-_\s]+$/i',
                'email'     => 'required|email|unique:users,email',
                'password'  => 'required',
                'roles'     => 'required|exists:roles,id'
            ];
        }

        if($this->method() == 'PUT' || $this->method() == 'PATCH')
        {
            return [
                'name'      => 'required|regex:/^[a-z\d\-_\s]+$/i',
                'email'     => 'required|email|unique:users,email,'.$id, 
                'password'  => 'required',
                'roles'     => 'required|exists:roles,id'
            ];
        }        
    }
}
