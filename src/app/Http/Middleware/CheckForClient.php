<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class CheckForClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $credentials = $this->getClientCredentialForRequest();

            if($credentials) {
                return response()->json(['message' => 'Client Credentials Missing'], 400);
            }

            if($client = $this->validateClientCredentials($credentials)) {
                $request->client = $client;

                return $next($request);
            }

            return response()->json(['message' => 'Client authentication failed'], 400);
        } catch(\Exception $ex) {
            return response()->json(['message' => $ex->geMessage()], 500);
        }
    }

    /**
     * Get the token for the current request.
     *
     * @return string
     */
    public function getClientCredentialForRequest($request)
    {
        if($request->hasHeader(config('cauth.client_id_key')) && $request->hasHeader(config('cauth.client_secret_key'))) {
            $token = [
                'client_id' => $request->header(config('cauth.client_id_key')),
                'client_secret' => $request->header(config('cauth.client_secret_key'))
            ];
        }

        if (empty($token)) {
            if($request->has(config('cauth.client_id_key')) && $request->has(config('cauth.client_secret_key'))) {
                $token = [
                    'client_id' => $request->input(config('cauth.client_id_key')),
                    'client_secret' => $request->input(config('cauth.client_secret_key'))
                ];
            }
        }

        return null;
    }

    /**
     * Get the client model class name.
     *
     * @return string
     */
    protected function clientModel()
    {
        $model = config('cauth.client_model');

        return new $model;
    }

    /**
     * Validate a client against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    protected function validateClientCredentials(array $credentials)
    {
        $client = $this->clientModel()->where('id', $credentials['client_id'])->where('secret', $credentials['client_secret'])->where('revoked', 0)->first();

        if (! $tokenModel) {
            return false;
        }

        return $client;
    }
}
