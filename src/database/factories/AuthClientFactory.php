<?php

use App\Models\AuthClient;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(AuthClient::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'secret' => Str::random(32),
    ];
});
