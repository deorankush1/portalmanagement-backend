<?php

use Illuminate\Database\Seeder;

class AuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\AuthClient::class, 1)->create();
    }
}
