<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $db_permission = Permission::all()->pluck('name')->toArray();

        $permission = [
            [
                'display_name' => 'View Dashboard',
                'name'         => 'view-dashboard',
                'guard_name'   => 'api'
            ],
            /*Permissions*/
            [
                'display_name' => 'Create Permission',
                'name'         => 'create-permission',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'View Permission',
                'name'         => 'view-permission',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Edit Permission',
                'name'         => 'edit-permission',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Delete Permission',
                'name'         => 'delete-permission',
                'guard_name'   => 'api'
            ],
            /*Roles*/
            [
                'display_name' => 'Create Role',
                'name'         => 'create-role',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'View Role',
                'name'         => 'view-role',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Edit Role',
                'name'         => 'edit-role',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Delete Role',
                'name'         => 'delete-role',
                'guard_name'   => 'api'
            ],
            /*Admins*/
            [
                'display_name' => 'Create Admin',
                'name'         => 'create-admin',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'View Admin',
                'name'         => 'view-admin',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Edit Admin',
                'name'         => 'edit-admin',
                'guard_name'   => 'api'
            ],
            [
                'display_name' => 'Delete Admin',
                'name'         => 'delete-admin',
                'guard_name'   => 'api'
            ],
        ];

    	foreach ($permission as $key => $value)
    	{
            // if(!in_array($value['name'], $db_permission)){
    		  Permission::create($value);
            // }
    	}
    }
}
