<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $db_role = Role::all()->pluck('name')->toArray();
        $all_permissions = Permission::all();


        if(!in_array('admin', $db_role)) {

            /*Admin*/
            $admin_role = Role::create([
                'display_name' => 'Admin',
                'name' 		   => 'admin',
            ]);

            foreach($all_permissions as $permission) {
                $admin_role->givePermissionTo($permission);
            }
        }	
    }
}
