<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('/oauth/token', 'AuthController@issueToken');
// Route::delete('/oauth/token/revoke', 'AuthController@revokeToken');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/*Users*/
Route::apiResource('users', 'UserController');

/*Roles*/
Route::apiResource('roles', 'RoleController');

/*Permissions*/
Route::apiResource('permissions', 'PermissionController');

/*Pages*/
Route::apiResource('pages', 'PageController');
